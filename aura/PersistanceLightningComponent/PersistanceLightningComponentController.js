({
	doInit : function(component, event, helper) {
		 helper.getFacadesTypes(component);
	},

	saveChanges : function(component, event, helper) {
		helper.save(component);
		
	},

})