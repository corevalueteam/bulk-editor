({
	getFacadesTypes : function(component) {
		 var action = component.get('c.getFacades');
         action.setCallback(this,function(response){
         var state = response.getState();
            if (state === "SUCCESS") {
            	var records=[];
                var result = response.getReturnValue();
                component.set('v.records', records);
                component.set('v.facades', result);
            }
        });
        $A.enqueueAction(action);
    },

    save : function(component){
        var childComponent = [];
        if(component.find("typeItem")){
           childComponent = component.find("typeItem");
               if(childComponent.length!=undefined){
                    for(var i = 0;i < childComponent.length;i++){
                        childComponent[i].saveRecords();
                    }
                }
                else {
                     childComponent.saveRecords(); 
                } 
        }


    }
})