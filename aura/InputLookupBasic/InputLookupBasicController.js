({
    
    /**
    * Handle initialization
    */
    onInit : function(component, event, helper) {

        helper.init(component, event);

    },
    
    
    
	/**
	* Handle re-initialization
	*/
	onReInit : function(component, event, helper) {
		
		helper.reInit(component, event);
		
	},
	
	
	
	/**
	* Handle focusing on search term
	*/
	onSearchTermFocus : function(component, event, helper) {
		
		helper.handleSearchTermFocus(component, event);
		
	},
	
	
	
	/**
	* Handle losing focus from search term
	*/
	onSearchTermBlur : function(component, event, helper) {
		
		helper.handleSearchTermBlur(component, event);
		
	},
	
	
	
	/**
	* Disable search term blur
	*/
	disableSearchTermBlur : function(component, event, helper) {
	
		helper.disableSearchTermBlur(component);
		
	},
	
	
	
	/**
	* Enable search term blur
	*/
	enableSearchTermBlur : function(component, event, helper) {
		
		helper.enableSearchTermBlur(component);
		
	},
	
	
	
	/**
	* Handle changing of search term
	*/
	onSearchTermKeyUp : function(component, event, helper) {
		
		helper.handleSearchTermKeyUp(component, event);
		
	},
	
	
	
	/**
	* Handle record selection
	*/
	onRecordSelect : function(component, event, helper) {
		
		helper.handleRecordSelection(component, event);
		
	},
	
	
	
	/**
	* Handle selection removal
	*/
	onSelectionRemove : function(component, event, helper) {
		
		helper.handleSelectionRemoval(component, event);
		
	},
	
	
	
	/**
	* Handle new record click
	*/
	onNewRecordClick : function(component, event, helper) {
		
		helper.handleNewRecordClick(component, event);
		
	},
	
	
	
	/**
	* Handle notification about value being changed
	*/
	onValueChanged : function(component, event, helper) {
		
		helper.reInit(component, event);
		
	}


})