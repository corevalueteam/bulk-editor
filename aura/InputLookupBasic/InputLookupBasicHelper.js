({
	

	/**
    * Initialization (run only once)
    */
    init : function(component, event) {
    
    	// actions which should happen only once
    	{
    		
		}
    	
    	// call to re-init (should be the last one)
    	this.render(component, event);

    },
    
    
    
    /**
    * Re-initialization (run many times)
    */
    render : function(component, event) {

		// get record by id if value is provided as an attribute
		if (this.isValue(component)) {
			
			this.getRecordById(component);
			
			
		// otherwise wipe/reset selection data
		} else {
			
			this.wipeSelectionData(component);
		
		}
    	
    },
    
    
    
    /**
	* On search term key up - trigger records search if search term is long enough
	*/
	handleSearchTermKeyUp : function(component, event) {
		
		// long enough: fetch records by keyword
		if (this.isSearchTermLongEnough(component)) {
			
			this.getRecordsByKeyword(component);
		
		
		// not long enough: clear previous found records
		} else {
			
			this.wipeFoundRecords(component);
		}
		
	},
	
	
	
	/**
	* On search term focus - just show lookup menu
	*/
	handleSearchTermFocus : function(component, event) {
		
		// wipe validation error
		this.wipeValidationError(component);
		
		// show lookup menu
		this.showLookupMenu(component, event);
		
	},
	
	
	
	/**
	* On search term blur - if blur is not disabed then just close lookup menu
	*/
	handleSearchTermBlur : function(component, event) {
		
		// early exit - search term blur disabled
		if (this.isSearchTermBlurDisabled(component)) {
			return;
		}
		
		// validate selection (and set errors if any) on "leaving" the control
		this.validateSelection(component);
		
		this.closeLookupMenu(component, event);
		
	},
	
	
	
	/**
	* Handle record selection
	*/
	handleRecordSelection : function(component, event) {
		
		var foundRecords = component.get("v.foundRecords");

		// get selected record id
		var selectedRecordId = event.currentTarget.getAttribute("data-record-id");		
		
		// get whole selected record object from found records by id
		var selectedRecordIndex = this.findObjectInArrayByProperty(foundRecords, "id", selectedRecordId);
		
		
		// set selection data
		this.setSelectionData(component, foundRecords[selectedRecordIndex]);
		
		
		// validate selection (and set errors if any)
		if (this.validateSelection(component)) {
			
			// change value and notify about new selection made
			this.setValueAndNotify(
				component,
				this.getSelectedRecord(
					component
				).id
			);
			
		}
			
		
		// close dialog
		this.closeLookupMenu(component);
		
	},
	
	
	
	/**
	* Handle selection removal
	*/
	handleSelectionRemoval : function(component, event) {
		
		this.wipeSelectionData(component);
		
		
		// validate selection (and set errors if any) on "leaving" the control
		if (this.validateSelection(component)) {
			
			// change value and notify about new selection made
			this.setValueAndNotify(
				component,
				null		// new value, since wiping has just happened
			);
			
		}
		
	},
	
	
	
	/**
	* Handle new record click
	*/
	handleNewRecordClick : function(component, event) {
		
    	this.enqueueAttributeAction(
    		component,
    		"v.onNewRecord"
    	);
		
	},
    
    
	
    // get record by id
    getRecordById : function(component) {
    	var thisHelper = this;
    	var action = component.get('c.getRecordById');
         action.setParams({
                "objectName"    :       this.getObjectName(component),
                "recordId"      :       this.getValue(component)
            },);

         action.setCallback(this, function(response) {
            var state = response.getState();
                if (state === "SUCCESS") {
                    console.log("SUCCESS");
                     var foundRecord = response.getReturnValue();
                     thisHelper.setSelectionData(component, foundRecord);
                }
                else {
                    console.log("Failed with state: " + state);
                }
            });
         $A.enqueueAction(action);
    },
    
    
    
    // get records by keyword and filter condition
    getRecordsByKeyword : function(component) {
    	
    	var thisHelper = this;
    	
        var action = component.get('c.getRecordsByKeyword');
         action.setParams({
                "objectName"        :       this.getObjectName(component),
                "keyword"           :       this.getSearchTerm(component),
                "filter"            :       this.getFilterTerm(component),
                "recordsLimit"      :       this.getRecordsLimit(component)
            },);

         action.setCallback(this, function(response) {
            var state = response.getState();
                if (state === "SUCCESS") {
                     var foundRecords = response.getReturnValue();
                     component.set("v.foundRecords", foundRecords);
                     console.log(foundRecords);
                }
                else {
                    console.log("Failed with state: " + state);
                    //thisHelper.showErrorMessage("Error: fetch record by id","error");
                }
            });
         $A.enqueueAction(action);	
    },
    
    
    
    // notify parent about value change
    notifyAboutValueChange : function(component) {
    	
    	this.enqueueAttributeAction(
    		component,
    		"v.onChange"
    	);
    	
    },
    
    
    
    // set value and notify
    setValueAndNotify : function(component, newValue) {
    	
    	// early exit: don't do anything if value hasn't been changed actually (just in case)
    	if (this.getValue(component) === newValue) {
    		return;
    	}
    	
    	
    	component.set(
    		"v.value",
    		newValue
    	);
    	
    	
    	this.notifyAboutValueChange(component);
    	
    },
    
    
    
    // validate selection
    validateSelection : function(component) {
    	
		// wipe validation error
		this.wipeValidationError(component);
    	
    	var isValid = true;
    	
    	
    	// empty in required mode
    	if (
    		this.isRequired(component)
    		&&
    		$A.util.isEmpty(
    			this.getSearchTerm(component)
    		)
    	) {
    		
    		// set validation error
    		this.addValidationError(
    			component,
    			"Record should be chosen."
    		);
    		
    		
    		isValid = false;
    		
    	}
    	
    	
    	// invalid record choosen
    	if (
    		// search term is set
    		!$A.util.isEmpty(
    			this.getSearchTerm(component)
    		)
    		&&
    		// but nothing selected from suggested results
    		$A.util.isEmpty(
    			this.getSelectedRecord(component)
    		)
    	) {
    		
    		// set validation error
    		this.addValidationError(
    			component,
    			"An invalid record has been chosen."
    		);
    		
    		
    		isValid = false;
    		
    	}
    	
    	
    	return isValid;
    	
    },
    
    
    
    // wipe/reset selection data
    wipeSelectionData : function(component) {
    	
    	// clear selected record
		component.set(
			"v.selectedRecord", 
			null
		);
		
		// clear search term
		component.set(
			'v.searchTerm', 
			''
		);
		
		// clear found records
		this.wipeFoundRecords(component);
		
		
		// wipe validation error
		this.wipeValidationError(component);
		
		
		// enable search term blur
		this.enableSearchTermBlur(component);
    	
    },
    
    
    
    // set selection data
    setSelectionData : function(component, record) {
    	
    	// catch selected record
		component.set(
			"v.selectedRecord", 
			record
		);
		
		// set search term as record name
		component.set(
			'v.searchTerm', 
			record.name
		);
		
		// set found records to single selected record		
		component.set(
			'v.foundRecords', 
			[ 
				record 
			]
		);
		
		
		// wipe validation error
		this.wipeValidationError(component);
		
		
		// enable search term blur
		this.enableSearchTermBlur(component);
    	
    },
    
    
    
    // is value
    isValue : function(component) {
    	
    	return (
    		!$A.util.isEmpty(
				this.getValue(
					component
				)
			)
		);
    	
    },
    
    
    
    // get value
    getValue : function(component) {
    	
    	return component.get("v.value");
    	
    },
    
    
    
    // is selected record
    isSelectedRecord : function(component) {
    	
    	return (
			!$A.util.isEmpty(
				this.getSelectedRecord(
					component
				)
			)
    	);
    	
    },
    
    
    
    // get selected record
    getSelectedRecord : function(component) {
    	
    	return component.get("v.selectedRecord");
    	
    },
    
    
    
    // wipe found records
    wipeFoundRecords : function(component) {
    	
		component.set(
			'v.foundRecords', 
			null
		);
    	
    },
    
    
    
    // get records limit
    getRecordsLimit : function(component) {
    	
    	return component.get("v.recordsNumberInPreview");
    	
    },
    
    
    
    // get object name
    getObjectName : function(component) {
    	
    	return component.get("v.objectName");
    	
    },
	
	
	
    // is search term
    isSearchTerm : function(component) {
    	
    	return (
    		!$A.util.isEmpty(
				this.getSearchTerm(
					component
				)
			)
		);
    	
    },
	
	
	
    // get search term
    getSearchTerm : function(component) {
    	
    	return component.get("v.searchTerm");
    	
    },
    
    getFilterTerm : function (component) {
        return component.get("v.filter");
    },
    
    
    // set search term
    setSearchTerm : function(component, value) {
    	
    	return component.set("v.searchTerm", value);
    	
    },

    
    
    // get search term min length
    getSearchTermMinLength : function(component) {
    	
    	return component.get("v.minSearchTermLength");
    	
    },
    
    
    
    // is search term long enough to trigger search
    isSearchTermLongEnough : function(component) {
    	
    	return (
    		this.isSearchTerm(component)
    		&&
    		this.getSearchTerm(component).length >= this.getSearchTermMinLength(component)
    	);
    
    },
    
    
    
    // show lookup menu
    showLookupMenu : function(component) {

        $A.util.addClass(
    		this.getLookupContainer(component), 
    		'slds-is-open'
    	);
        
    },
    
    
    
    // close lookup menu
    closeLookupMenu : function(component) {
    	
    	$A.util.removeClass(
			this.getLookupContainer(component), 
			'slds-is-open'
    	);
    	
    },
    
    
    
    // get lookup container
    getLookupContainer : function(component) {
    	
    	return component.find('lookupContainer');
    	
    },
    
    
    
    // enable search term blur
    enableSearchTermBlur : function(component) {
    	
    	component.set(
    		"v.ignoreSearchTermBlur",
    		false
    	);
    	
    },
    
    
    
    // disable search term blur
    disableSearchTermBlur : function(component) {
    	
    	component.set(
			"v.ignoreSearchTermBlur",
			true
    	);
    	
    },
    
    
    
    // is search term blur enabled
    isSearchTermBlurEnabled : function(component) {
    	
    	return !this.isSearchTermBlurDisabled(component);
    	
    },
    
    
    
    // is search term blur disabled
    isSearchTermBlurDisabled : function(component) {
    	
    	return component.get("v.ignoreSearchTermBlur") === true;
    	
    },
    
    
    
    // get search term control
    getSearchTermControl : function(component) {
    	
    	return component.find("searchTermControl");
    	
    },
    
    
    
    // wipe validation error
    wipeValidationError : function(component) {
    	
    	this.clearComponentError(
			component
    	);
    
    },
    
    clearComponentError : function(component) {
        
        component.set(
            "v.errors",
            null
        );
        
    },
    
    // set validation error
    setValidationError : function(component, error) {
    	
    	this.setComponentError(
    		component,
			error
    	);
    	
    },
    
    
    
    // add validation error
    addValidationError : function(component, error) {
    	
    	/*this.addComponentError(
			component,
			error
    	);*/
    	
    },
    
    enqueueAttributeAction : function(component, attributeName) {
    
        var attributeAction = component.get(attributeName);
        
        if (!$A.util.isEmpty(attributeAction)) {
            
            this.enqueueAction(attributeAction);
        
        }
        
    },

     findObjectInArrayByProperty : function(array, propertyName, propertyValue) {
        
        var resultIndex = -1;
        
        
        for (var i in array) {
            
            if (array[i][propertyName] === propertyValue) {
                
                resultIndex = i;
                
                break;
                
            }
            
        }
        
        
        return resultIndex;
        
    },


    
    // check whether value is required
    isRequired : function(component) {
    	
    	return component.get("v.required") === true;
    	
    }
    
	
})