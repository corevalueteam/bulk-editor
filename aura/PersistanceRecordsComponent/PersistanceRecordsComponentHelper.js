({
	onInit : function(component) { 
		var key = component.get("v.key");
		var facades = component.get("v.facades");
		var indexKey = component.get("v.index");
		var mapRecords = facades[indexKey].typeRecords;
		var recordName = facades[indexKey].recordName;
		var fields =  facades[indexKey].typeFields;
		var labels =  facades[indexKey].fieldLabels;
		var typeFields = fields[key];
		var records = [];
		var fieldsList = [];
		var fieldProperties = facades[indexKey].recordFieldsProperties;

			for (let item in mapRecords[key]){
				fieldsList = [];
					for (let index in typeFields){
						fieldsList.push({fieldName:typeFields[index], label: labels[typeFields[index]], 
																	fieldValue:mapRecords[key][item][typeFields[index]], 
																	properties:fieldProperties[typeFields[index]]});
					}
				records.push({record:mapRecords[key][item], recordName:mapRecords[key][item][recordName], values:fieldsList});
			}
		component.set("v.records",records)
		//component.set("v.fieldsList", fieldsList);
	},


	saveRecords : function(component) {
		var key = component.get("v.key");
		var facades = component.get("v.facades");
		var index = component.get("v.index");
		var recordList = [];
		var recordListItems = [];
		var records = component.get("v.records");
		var fieldsList = component.get("v.fieldsList");
		for(let i =0; i<facades[index].typeRecords[key].length; i++){
			for(let j = 0; j<records[i].values.length; j++){
				facades[index].typeRecords[key][i][records[i].values[j].fieldName] = records[i].values[j].fieldValue;
			}
		}
		var action = component.get('c.saveChanges');
		action.setParams({ 
		 	"facadeEntity" : JSON.stringify( facades[index]) 
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
                if (state === "SUCCESS") {
                    $A.get('e.force:refreshView').fire();
                }
                else {
                    console.log("Failed with state: " + state);
                }
            });
		 $A.enqueueAction(action);
	} 
})