/**
 *  @Description Controller for PersistanceLightningApp
 *  @author Maksym Gorinshteyn
*/
public class PersistanceLightningAppController {

	@AuraEnabled
	public static List<PersistanceFacade> getFacades(){
		List<PersistanceFacade> facades = new List<PersistanceFacade>();
		PersistanceMediator facadeManager = new PersistanceMediator();
		facades = facadeManager.getTransportFacades();
	return facades;
	}

	@AuraEnabled
	public static void saveChanges(String facadeEntity){
		/*System.debug(facadeEntity);
		PersistanceFacade facade = (PersistanceFacade)System.JSON.deserialize(facadeEntity, PersistanceFacade.class); 
		System.debug(facade);*/

		Map<String, Object> facadeItemMap = (Map<String, Object>)JSON.deserializeUntyped(facadeEntity); 
		PersistanceFacade facade  = new PersistanceFacade();

		facade.BEPIdentifier =(Integer) facadeItemMap.get('BEPIdentifier');

		List<Object>tempTypesList = (List<Object>) facadeItemMap.get('types');
		facade.types =  new List<PersistanceFacade.PersistanceFacadeItem>();
			for(Object tempType :tempTypesList ){
			  	 Map<String, Object> tempTypeMap= (Map<String, Object>) tempType;
			  	 PersistanceFacade.PersistanceFacadeItem typeItem = new PersistanceFacade.PersistanceFacadeItem();
			  	 typeItem.typeLabel = (String) tempTypeMap.get('typeLabel');
			  	 typeItem.typeName = (String)tempTypeMap.get('typeName');
			  	 typeItem.isSelected = (Boolean)tempTypeMap.get('isSelected');
				 facade.types.add(typeItem);
			}

		Map<String, Object> tempTypeRecords = (Map<String, Object>) facadeItemMap.get('typeRecords');
		facade.typeRecords =  new Map<String, List<Object>>();
		for(String key: tempTypeRecords.keySet()){ 		
		 	for(PersistanceFacade.PersistanceFacadeItem typeItem: facade.types){
		 		if(key==typeItem.typeLabel){
		 			sObject sObj = Schema.getGlobalDescribe().get(typeItem.typeName).newSObject();
		 			String listType = 'List<' + sObj.getSObjectType() + '>';
		 			List<SObject> castRecords = (List<SObject>)Type.forName(listType).newInstance();
		 			String str = JSON.serializePretty(tempTypeRecords.get(key));
		 			castRecords = (List<sObject>)System.JSON.deserialize(str, Type.forName(listType));
		 			facade.typeRecords.put(key, (List<Object>)castRecords);
		 		}
		 	}
		}
		 System.debug(facade);
		 facade.upsertCustomTypesRecords(facade);
    }
}