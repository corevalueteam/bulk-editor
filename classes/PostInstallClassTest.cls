@isTest
private class PostInstallClassTest {
	
	@isTest
	static void testInstallScript() {
	  PostInstallClass postinstall = new PostInstallClass();
	    Test.testInstall(postinstall, null);
	    CV_BulkElementsEditorConfiguration__c configuration = 
	    									CV_BulkElementsEditorConfiguration__c.getValues('BEditor_Custom_Settings_Configuration');
	    System.assertEquals(configuration.Implementation_class_name__c, 'CustomSettingsPersistanceImpl');
	  }
	
}