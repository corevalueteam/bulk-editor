@isTest
private class PersistancePageControllerTest {
    
    static PersistancePageController controller;
    static List<EditableEntity> bulkElementPersistanceList= new List<EditableEntity>{
        new CustomSettingsPersistanceImpl()
    };

    @isTest 
    static void validateBulkElementPersistancePageController() {
        for(EditableEntity bulkElementPersistance:bulkElementPersistanceList ){
            bulkElementPersistance.createTestData(20);
        }
        controller = new PersistancePageController();
        System.assertEquals(controller.facades.size(), 
                                                PersistanceMediator.getAvailableImplementationsCFG().size()); 

    }

    @isTest 
    static void validateSaveChanges() {
        for(EditableEntity bulkElementPersistance:bulkElementPersistanceList ){
            bulkElementPersistance.createTestData(20);
            controller = new PersistancePageController();
            if(bulkElementPersistance instanceof CustomSettingsPersistanceImpl){
                controller.saveChanges();
                List<CV_BulkElementsEditorConfiguration__c> testRecords = [SELECT Id, Name, Implementation_class_name__c FROM CV_BulkElementsEditorConfiguration__c];
                System.assertEquals(testRecords.size(),21);
            }
        }           
    }       
}