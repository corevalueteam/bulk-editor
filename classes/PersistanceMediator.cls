/**
*   @Description governs the relations between instances of EditableEntity interface and their respective PersistanceFacade facades
*   @author Maksym Gorinshteyn
*/

public class PersistanceMediator{

        /**@Description configuration of EditableEntity interface implementations */
        private static List<EditableEntity> availableImplementationsCFG{
            get{
                if(availableImplementationsCFG==null){
                    availableImplementationsCFG = new List<EditableEntity>();
                    List<CV_BulkElementsEditorConfiguration__c> configurations = 
                                                        CV_BulkElementsEditorConfiguration__c.getAll().values(); 
                    Set<String> uniqueImplementationNames = new Set<String>();
                    for(CV_BulkElementsEditorConfiguration__c configuration:configurations){
                         uniqueImplementationNames.add(configuration.Implementation_class_name__c);
                    }
                    for(String implementationName : new List<String>(uniqueImplementationNames)){
                        Type config= Type.forName(implementationName);    
                        if(config!=null){
                            EditableEntity implementation =(EditableEntity)config.newInstance();
                            availableImplementationsCFG.add(implementation);
                        }
                    }
                }
                return availableImplementationsCFG;
            }
        }

        
        private List<PersistanceFacade> facadesOfImplementations = new List<PersistanceFacade>();

        public PersistanceMediator() {
            this.convert(availableImplementationsCFG);
        }

        /** 
        *   @Description used to convert implementations of EditableEntity interface to their respective PersistanceFacade facades
        *   @param implementations EditableEntity implementations
        */
        private void convert(List<EditableEntity> implementations){
            for(EditableEntity implementation : implementations){
                PersistanceFacade facade = new PersistanceFacade(implementation);
                facadesOfImplementations.add(facade);
            }
        }


        public static List<EditableEntity> getAvailableImplementationsCFG(){
            return availableImplementationsCFG;
        }


        public static Integer getBEPIdentifier(EditableEntity implementation){
            return availableImplementationsCFG.indexOf(implementation);
        }


        public static EditableEntity getImplementation(Integer BEPIdentifier){
            return availableImplementationsCFG.get(BEPIdentifier);
        }


        public List<PersistanceFacade> getTransportFacades(){
            return facadesOfImplementations;
        }
    }