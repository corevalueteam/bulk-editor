/**
 *  @Description interface for Custom Implementations
 *  @author Maksym Gorinshteyn
*/
global interface EditableEntity {
    
	/** @Description used to retrieve types of configuration data */
    List<Schema.DescribeSObjectResult> getCustomTypes();

    /** @Description used to retrieve fields of retrieved types of configuration data  */
    Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>> getCustomTypeFields();

    /** @Description used to retrieve records of retrieved types of configuration data  */
    Map<Schema.DescribeSObjectResult, List<Object>> getCustomTypeRecords();

    void upsertCustomTypeRecords(List<Object> recordList);

    String getConfigurationLabel();

    String getRecordName();

    /** @Description used by universal Unite Test, which tests all known implementations of the interface uniformly */
    void createTestData(Integer numberOfRecords);
}