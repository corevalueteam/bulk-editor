/**
 *  @Description Controller for PersistanceVisualforcePage
 *  @author Maksym Gorinshteyn
*/

public class PersistancePageController {

        public List<PersistanceFacade> facades {get;set;}

        public PersistancePageController(){
            PersistanceMediator facadeManager = new PersistanceMediator();
            facades = facadeManager.getTransportFacades();
        }

        public PageReference saveChanges(){
            for(PersistanceFacade facade : facades){
                facade.upsertCustomTypesRecords(facade);
            }
            PageReference pr = new PageReference('/apex/PersistanceVisualforcePage');
            pr.setRedirect(true);
            return pr;
        }
}