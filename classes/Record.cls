/**
* Record data model in general, i.e. without referring to specific sobject type.
*/
public class Record {
	
	/**
	* Id
	*/
	@AuraEnabled
	public Id id {
		get;
		private set;
	}
	
	
	/**
	* Name
	*/
	@AuraEnabled
	public String name {
		get;
		private set;
	}
	
	
	
	public Record(Id id, Object name) {
		
		this.id = id;
		this.name = String.valueOf(name);
		
	}		
}