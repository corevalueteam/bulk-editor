/**
 *  @Description converted instances of EditableEntity interface to their respective PersistanceFacade facades
 *  @author Maksym Gorinshteyn
*/

public class PersistanceFacade {

	@AuraEnabled public List<PersistanceFacadeItem> types{get; set;}
	@AuraEnabled public Map<String, List<String>> typeFields{get; set;}
	@AuraEnabled public Map<String, List<Object>> typeRecords{get;set;}

	/*
	* Map Field name => Field label 
	*/
	@AuraEnabled public Map<String, String> fieldLabels{get; set;}
	@AuraEnabled public String configurationLabel{get; set;}
	@AuraEnabled public String recordName{get; set;}
	@AuraEnabled public Integer BEPIdentifier;
	
	/*
	* Map Field name => List of properties of each field (isUpdateable(), getType(), isNillable()) 
	*/
	@AuraEnabled public Map<String, FieldWrapper> recordFieldsProperties{get;set;}


	public PersistanceFacade() {}

	public PersistanceFacade(EditableEntity implementation) {

		this.recordFieldsProperties = new Map<String, FieldWrapper>();
		this.fieldLabels = new  Map<String, String>();
		this.types = getTypeLabels(implementation);
		this.typeRecords = getTypeRecords(implementation);
		this.typeFields = getTypeFields(implementation);
		this.BEPIdentifier = PersistanceMediator.getBEPIdentifier(implementation);
		this.configurationLabel = implementation.getConfigurationLabel();
		this.recordName = implementation.getRecordName();	
	}

	private List<PersistanceFacadeItem> getTypeLabels(EditableEntity implementation){
		List<PersistanceFacadeItem> customTypesLabels = new List<PersistanceFacadeItem>();
		List<Schema.DescribeSObjectResult> customTypes = implementation.getCustomTypes();
			for(Schema.DescribeSObjectResult customType:customTypes){
			  customTypesLabels.add(new PersistanceFacadeItem(customType.getLabel(), customType.getName()));
			}	
		return customTypesLabels;
	}

	private Map<String, List<String>> getTypeFields(EditableEntity implementation){
		Map<String, List<String>> customTypesFields = new Map<String, List<String>>();
		Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>> mapFields = 
																			implementation.getCustomTypeFields();
			for(Schema.DescribeSObjectResult mapField:mapFields.keySet()){
				List<String> fieldsList  = new List<String>();
				for (Schema.DescribeFieldResult field: mapFields.get(mapField)){
					FieldWrapper fieldProperties = new FieldWrapper();
					fieldProperties.isUpdateable =field.isUpdateable();
					fieldProperties.fieldType = String.valueOf(field.getType());
					fieldProperties.isNillable = field.isNillable();
					fieldProperties.referenceTo = String.valueOf(field.getReferenceTo()).remove('(').remove(')');
					fieldsList.add(field.getName());
					fieldLabels.put(field.getName(), field.getLabel());

					List<List<String>> selectOptions = new List<List<String>>();
					if(String.valueOf(field.getType())=='PICKLIST'){
						List<Schema.PicklistEntry> pickListEntries = field.getPicklistValues();
						for( Schema.PicklistEntry pickListEntry : pickListEntries){
						    selectOptions.add(new List<String>{pickListEntry.getLabel(),pickListEntry.getValue()});
						}
						fieldProperties.pickListValues = selectOptions;
						      
					}
					recordFieldsProperties.put(field.getName(), fieldProperties);
				}

				customTypesFields.put(mapField.getLabel(),fieldsList);
			}
		return customTypesFields;
	}

	private Map<String, List<Object>> getTypeRecords(EditableEntity implementation){
		Map<String, List<Object>> typeRecords = new Map<String, List<Object>>();
		Map<Schema.DescribeSObjectResult, List<Object>> mapRecords= implementation.getCustomTypeRecords();
			for(Schema.DescribeSObjectResult mapRecord: mapRecords.keySet()){
				typeRecords.put(mapRecord.getLabel(), mapRecords.get(mapRecord));
			}
		return typeRecords;
	}

	public void upsertCustomTypesRecords(PersistanceFacade facade){
		System.debug(facade.typeRecords);
		for (PersistanceFacade.PersistanceFacadeItem item: facade.types){
     		List<Object> recordList = typeRecords.get(item.typeLabel);
     		EditableEntity implementation = PersistanceMediator.getImplementation(BEPIdentifier);
     		implementation.upsertCustomTypeRecords(recordList);
		}
	}

	public class PersistanceFacadeItem{
		@AuraEnabled public String typeLabel{get;set;}
		@AuraEnabled public String typeName{get;set;}
		@AuraEnabled public Boolean isSelected{get;set;}

		public PersistanceFacadeItem(){}

		public PersistanceFacadeItem(String typeLabel, String typeName){
			this.typeLabel = typeLabel;
			this.typeName = typeName;
			this.isSelected = false;
		}
	}

	public class FieldWrapper{
		@AuraEnabled public Boolean isUpdateable{get;set;}
		@AuraEnabled public String fieldType{get;set;}
		@AuraEnabled public Boolean isNillable{get;set;}
		@AuraEnabled public List<List<String>> pickListValues{get;set;}
		@AuraEnabled public String referenceTo{get;set;}

		public FieldWrapper(){}
	}
}