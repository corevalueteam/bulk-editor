@isTest
private class EditableEntityTest {
	
	static List<EditableEntity> bulkElementPersistanceList= new List<EditableEntity>{
        new CustomSettingsPersistanceImpl()
    };
    
    static List<Schema.DescribeSObjectResult> customSettingTypes {
        get {
            if(customSettingTypes == null){
                customSettingTypes = new List<Schema.DescribeSObjectResult>();
                for (Schema.SObjectType sObjectType : Schema.getGlobalDescribe().values()) {
                    Schema.DescribeSObjectResult objResult = sObjectType.getDescribe();
                    if(objResult.isCustomSetting()){
                        customSettingTypes.add(objResult);
                    }
                }
            }
            return customSettingTypes;
        }
    }

    static Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>> mapCustomSettingFields {
        get{
            if(mapCustomSettingFields==null){
            mapCustomSettingFields = new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>();
                for (Schema.DescribeSObjectResult customSettingType: customSettingTypes){
                List<Schema.DescribeFieldResult> customSettingFieldsList  = new List<Schema.DescribeFieldResult>();
                    for(Schema.SObjectField field : customSettingType.fields.getMap().values() ){
                        customSettingFieldsList.add(field.getDescribe());
                    }
                mapCustomSettingFields.put(customSettingType, customSettingFieldsList);
                }
            }   
            return mapCustomSettingFields;
        }
    }

        
    @isTest
    static void validateGetCustomTypes(){
        for (EditableEntity bulkElementPersistance: bulkElementPersistanceList){
            List<Schema.DescribeSObjectResult> customTypes = bulkElementPersistance.getCustomTypes();
                if(bulkElementPersistance instanceof CustomSettingsPersistanceImpl){
                    System.assertEquals(customTypes.size(),customSettingTypes.size());

                }
        }

    }

    @isTest
    static void validateGetCustomTypeFields(){
        for (EditableEntity bulkElementPersistance: bulkElementPersistanceList){
            Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>> customTypeFields=
                                                                            bulkElementPersistance.getCustomTypeFields();
            if(bulkElementPersistance instanceof CustomSettingsPersistanceImpl){
                System.assertEquals(customTypeFields.keySet().size(),mapCustomSettingFields.keySet().size());
                System.assertEquals(customTypeFields.values().get(0).size(), mapCustomSettingFields.values().get(0).size());
            }
        }

    }
    
    @isTest
    static void validateGetCustomTypeRecords(){
        for (EditableEntity bulkElementPersistance: bulkElementPersistanceList){
            bulkElementPersistance.createTestData(65);
            Map<Schema.DescribeSObjectResult, List<Object>> customTypeRecords = bulkElementPersistance.getCustomTypeRecords();
                if(bulkElementPersistance instanceof CustomSettingsPersistanceImpl){
                    System.assertEquals(customTypeRecords.keySet().size(), mapCustomSettingFields.keySet().size());
                }
        }

    }

    /**
    * @Description method tests in PersistanceFacadeTest class
    */
    @isTest
    static void validateUpsertCustomTypeRecords(){}
}