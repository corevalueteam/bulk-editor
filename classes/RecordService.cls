public with sharing class RecordService {

	public static Schema.DescribeFieldResult getObjectNameField(String objectName) {
        Schema.SObjectType sObjectType =  Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult describeSObjResult =  sObjectType.getDescribe();
        // loop through fields and find first named one
    	for(Schema.SObjectField field : describeSObjResult.fields.getMap().values()){
            if (field.getDescribe().isNameField()){ 
                return field.getDescribe();
            }
        }
        return null;   
    }

    public static Record getRecordById(String objectName, Id recordId){
    	String objectNamedField = getObjectNameField(objectName).getName();
		sObject sObj;
    	try {
    		String query = String.format(
    										'SELECT Id, {0} FROM {1} WHERE Id=\'\'{2}\'\' LIMIT 1', 
    										new String[]{
    														objectNamedField, 
    														objectName,
    														recordId
    													}
    									);
			sObj =Database.query(query);
			return new Record((Id)sObj.get('Id'), sObj.get('Name'));
		} catch (Exception exp) {
			throw new AuraHandledException(
				exp.getMessage()
			);
		}	
    }

    public static List<Record> getRecordsByKeyword(String objectName, String keyword, String filter, Decimal recordsLimit){
    	String objectNamedField = getObjectNameField(objectName).getName();
		List<sObject> sObjs = new List<sObject>();
		List<Record> records = new List<Record>();
		try {
			String query = String.format(
										'SELECT Id, {0} FROM {1} WHERE {0} LIKE \'\'%{2}%\'\' LIMIT '+recordsLimit,
										new String[]{
														objectNamedField,
														objectName,
														keyword
													}
										);
			sObjs =Database.query(query);
			for(sObject sObj:sObjs){
				records.add( new Record((Id)sObj.get('Id'), sObj.get('Name')));
			}
			return records;
		} catch (Exception exp) {
			throw new AuraHandledException(
				exp.getMessage()
			);
		}				
		return null;
	}
}
           
           
