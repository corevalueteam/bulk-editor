/**
 *  @Description Implementation of EditableEntity  interface for Users
 *  @author Maksym Gorinshteyn
*/
public class UsersPersistanceImpl implements EditableEntity{

	public UsersPersistanceImpl() {
        this.usersMetadata = usersMetadata;
        this.mapUsersFields = mapUsersFields;
    }


    private List<Schema.DescribeSObjectResult> usersMetadata {
        get {
            if(usersMetadata == null){
                usersMetadata = new List<Schema.DescribeSObjectResult>();
                for (Schema.SObjectType sObjectType : Schema.getGlobalDescribe().values()){
                    Schema.DescribeSObjectResult objResult = sObjectType.getDescribe();
                    if(objResult==Schema.SObjectType.User){
                        usersMetadata.add(objResult);
                    }
                }
            }
            return usersMetadata;
        }
        set;
    }
     /*
     * Map User Metadata type => List of Metadata of fields for User type 
     */
    private Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>> mapUsersFields {
        get{
            if(mapUsersFields==null){
            mapUsersFields = new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>();
                for (Schema.DescribeSObjectResult user: usersMetadata){
                List<Schema.DescribeFieldResult> mapUsersFieldsList  = new List<Schema.DescribeFieldResult>();
                    for(Schema.SObjectField field : user.fields.getMap().values() ){
                        if((String.valueOf(field)!='Address') && (String.valueOf(field)!='LastModifiedById')){
                            mapUsersFieldsList.add(field.getDescribe());
                        }
                    }
                mapUsersFields.put(user, mapUsersFieldsList);
                }
            }   
            return mapUsersFields;
        }
        set;
    }

    private static final String CONFIGURATION_LABEL= 'Users';

    private static final String RECORD_NAME = 'Name';


    public List<Schema.DescribeSObjectResult> getCustomTypes(){
        return usersMetadata;
    }
    
    public Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>> getCustomTypeFields(){
        return mapUsersFields;
    }

    public String getConfigurationLabel(){
        return CONFIGURATION_LABEL;
    }


    public String getRecordName(){
        return RECORD_NAME;
    }

    /** 
    *Map User Metadata type => all records for each type
    */
    public Map<Schema.DescribeSObjectResult, List<Object>> getCustomTypeRecords(){
        Map<Schema.DescribeSObjectResult, List<Object>> mapUserRecords = 
                                                                    new Map<Schema.DescribeSObjectResult, List<Object>>();

        for (Schema.DescribeSObjectResult user: usersMetadata){
            List<String> fieldNames = new List<String>();
            List<Schema.DescribeFieldResult> fields = mapUsersFields.get(user);
                for(Schema.DescribeFieldResult field:fields){
                    fieldNames.add(field.getName());
                }       
            String queryFields = String.join(fieldNames, ',');
            List<Object> usersPerType =  Database.query('SELECT '+queryFields+' FROM ' + user.getSObjectType());
            mapUserRecords.put(user, usersPerType);
        }
        return mapUserRecords;
    }

    public void upsertCustomTypeRecords(List<Object> recordList){
        List<SObject> upsertRecords = (List<SObject>)recordList;
        upsert upsertRecords;
    }
   
    /** @Description used by universal Unite Test, which tests all known implementations of the interface uniformly */
    public void createTestData(Integer numberOfRecords){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        List<User> testUsers = new List<User>();
            for (Integer i=0; i<numberOfRecords; i++){
                testUsers.add(new User(LastName = 'TestName'+i,
                                       Email = 'test'+i+'@test.com',
                                       Alias = 'Talias'+i,
                                       Username = 'forTest'+i+'@test.com',
                                       CommunityNickname = 'test'+i,
                                       LocaleSidKey = 'en_US',
                                       TimeZoneSidKey = 'GMT',
                                       ProfileID = profileId.Id,
                                       LanguageLocaleKey = 'en_US',
                                       EmailEncodingKey = 'UTF-8'));
            }
        insert testUsers;
    }
}