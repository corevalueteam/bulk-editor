@isTest
private class PersistanceFacadeTest {
    

    static List<EditableEntity> bulkElementPersistanceList= PersistanceMediator.getAvailableImplementationsCFG();

    static List<Schema.DescribeSObjectResult> customSettingTypes {
            get {
                if(customSettingTypes == null){
                    customSettingTypes = new List<Schema.DescribeSObjectResult>();
                    for (Schema.SObjectType sObjectType : Schema.getGlobalDescribe().values()) {
                        Schema.DescribeSObjectResult objResult = sObjectType.getDescribe();
                        if(objResult.isCustomSetting()){
                            customSettingTypes.add(objResult);
                        }
                    }
                }
                return customSettingTypes;
            }
    }

    @isTest 
    static void validateBulkElementPersistanceTransports(){
        for (EditableEntity bulkElementPersistance:bulkElementPersistanceList){
            PersistanceFacade bulkElementPersistanceTransport= new PersistanceFacade(bulkElementPersistance);
                if(bulkElementPersistance instanceof CustomSettingsPersistanceImpl){
                    System.assertEquals(bulkElementPersistanceTransport.types.size(), customSettingTypes.size());
                }
        }
    }

    
    @isTest 
    static void validateUpsertCustomTypesRecords(){
        Integer expectedRecordsNumber = 21;
        for (EditableEntity bulkElementPersistance:bulkElementPersistanceList){
            bulkElementPersistance.createTestData(20);
            PersistanceFacade bulkElementPersistanceTransport= new PersistanceFacade(bulkElementPersistance);
                if(bulkElementPersistance instanceof CustomSettingsPersistanceImpl){
                    List<CV_BulkElementsEditorConfiguration__c> testTypes = [SELECT Id, Name, Implementation_class_name__c FROM CV_BulkElementsEditorConfiguration__c];
                    bulkElementPersistanceTransport.typeRecords.values().get(1).add(new CV_BulkElementsEditorConfiguration__c(Name = 'TestName'));
                    bulkElementPersistanceTransport.upsertCustomTypesRecords(bulkElementPersistanceTransport);
                    List<CV_BulkElementsEditorConfiguration__c> testRecords = [SELECT Id, Name, Implementation_class_name__c FROM CV_BulkElementsEditorConfiguration__c];
                    System.assertEquals(expectedRecordsNumber,testRecords.size());

                }
        }
    }
    
}