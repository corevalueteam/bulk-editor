/**
* Controller for InputLookupBasic lightning component
*/
public with sharing class InputLookupBasicCmpController {

	//Get record by id
	@AuraEnabled
	public static Record getRecordById(String objectName, Id recordId) {
		return  RecordService.getRecordById(objectName, recordId);
	}
	
	// Get records by keyword
	@AuraEnabled
	public static List<Record> getRecordsByKeyword(String objectName, String keyword, String filter, Decimal recordsLimit) {
		return  RecordService.getRecordsByKeyword(objectName, keyword, filter, recordsLimit);
	}
	
}