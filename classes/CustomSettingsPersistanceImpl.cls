/**
 *  @Description Implementation of EditableEntity  interface for Custom Settings
 *  @author Maksym Gorinshteyn
*/
public class CustomSettingsPersistanceImpl implements EditableEntity{

/*
    public CustomSettingsPersistanceImpl() {
        this.customSettingsMetadataTypes = customSettingsMetadataTypes;
        this.mapCustomSettingFields = mapCustomSettingFields;
        }
    */

    private static final String CONFIGURATION_LABEL= 'Custom Settings';

    private static final String RECORD_NAME = 'Name';

    private List<Schema.DescribeSObjectResult> customSettingsMetadataTypes {
        get {
            if(customSettingsMetadataTypes == null){
                customSettingsMetadataTypes = new List<Schema.DescribeSObjectResult>();
                for (Schema.SObjectType sObjectType : Schema.getGlobalDescribe().values()) {
                    Schema.DescribeSObjectResult objResult = sObjectType.getDescribe();
                    if(objResult.isCustomSetting()){
                        customSettingsMetadataTypes.add(objResult);
                    }
                }
            }
            return customSettingsMetadataTypes;
        }
        set;
    }

    /** 
    * Map Custom Setting Metadata type => List Metadata of fields for each Metadata type 
    */
    private Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>> mapCustomSettingFields {
        get{
            if(mapCustomSettingFields==null){
            mapCustomSettingFields = new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>();
                for (Schema.DescribeSObjectResult customSettingType: customSettingsMetadataTypes){
                    List<Schema.DescribeFieldResult> customSettingFieldsList  = new List<Schema.DescribeFieldResult>();
                    for(Schema.SObjectField field : customSettingType.fields.getMap().values() ){
                        customSettingFieldsList.add(field.getDescribe());
                    }
                    mapCustomSettingFields.put(customSettingType, customSettingFieldsList);
                }
            }   
            return mapCustomSettingFields;
        }
        set;
    }

    public List<Schema.DescribeSObjectResult> getCustomTypes(){
        return customSettingsMetadataTypes;
    }

    public Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>> getCustomTypeFields(){
        return mapCustomSettingFields;
    }

    public String getConfigurationLabel(){
        return CONFIGURATION_LABEL;
    }

    public String getRecordName(){
        return RECORD_NAME;
    }

    /** 
    *Map Custom Settings Metadata type => all records for each type
    */
    public Map<Schema.DescribeSObjectResult, List<Object>> getCustomTypeRecords(){
        Map<Schema.DescribeSObjectResult, List<Object>> mapCustomSettingRecords = 
                                                                    new Map<Schema.DescribeSObjectResult, List<Object>>();

        for (Schema.DescribeSObjectResult customSettingType: customSettingsMetadataTypes){
            List<String> fieldNames = new List<String>();
            List<Schema.DescribeFieldResult> fields = mapCustomSettingFields.get(customSettingType);
                for(Schema.DescribeFieldResult field:fields){
                    fieldNames.add(field.getName());
                }       
            String queryFields = String.join(fieldNames, ',');
            List<Object> records =  Database.query('SELECT '+queryFields+' FROM ' + customSettingType.getSObjectType());
            mapCustomSettingRecords.put(customSettingType, records);
        }
        return mapCustomSettingRecords;
    }

    public  void upsertCustomTypeRecords(List<Object> recordList){
        upsert (List<SObject>)recordList;
    }

    
    /** 
    * used by universal Unite Test, which tests all known implementations of the interface uniformly 
    */
    public void createTestData(Integer numberOfRecords){
        List<CV_BulkElementsEditorConfiguration__c> testTypes = new List<CV_BulkElementsEditorConfiguration__c>();
            for (Integer i=0; i<numberOfRecords; i++){
                testTypes.add(new CV_BulkElementsEditorConfiguration__c(Name ='BulkEditorConfiguration'+i,
                                                             Implementation_class_name__c = 'Implementation'+i));
            }
        insert testTypes;
        CV_BulkElementsEditorConfiguration__c testConfiguration = new CV_BulkElementsEditorConfiguration__c(Name = 'configuration', Implementation_class_name__c ='CustomSettingsPersistanceImpl');
        insert testConfiguration;
    }    
}