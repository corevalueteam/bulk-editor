global class PostInstallClass implements InstallHandler {

	global void onInstall(InstallContext context) {
    	if(context.previousVersion() == null) {
    		CV_BulkElementsEditorConfiguration__c configuration = 
    										new CV_BulkElementsEditorConfiguration__c(Name ='BEditor_Custom_Settings_Configuration', 
    										Implementation_class_name__c = 'CustomSettingsPersistanceImpl');
    		insert configuration;
    	}
	}
}